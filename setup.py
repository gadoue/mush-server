#!/usr/bin/env python
# Copyright (c) 2022, Guillaume Gay
#
# Distributed under the 3-clause BSD license, see accompanying file LICENSE
# or https://gitlab.com/gadoue/mush-server for details.

from __future__ import annotations

from setuptools import setup

setup()

# This file is optional, on recent versions of pip you can remove it and even
# still get editable installs.

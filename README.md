# mush-server

[![Actions Status][actions-badge]][actions-link]
[![Documentation Status][rtd-badge]][rtd-link]
[![Code style: black][black-badge]][black-link]

[![PyPI version][pypi-version]][pypi-link]
[![Conda-Forge][conda-badge]][conda-link]
[![PyPI platforms][pypi-platforms]][pypi-link]

[![GitHub Discussion][github-discussions-badge]][github-discussions-link]
[![Gitter][gitter-badge]][gitter-link]




[actions-badge]:            https://gitlab.com/gadoue/mush-server/workflows/CI/badge.svg
[actions-link]:             https://gitlab.com/gadoue/mush-server/actions
[black-badge]:              https://img.shields.io/badge/code%20style-black-000000.svg
[black-link]:               https://github.com/psf/black
[conda-badge]:              https://img.shields.io/conda/vn/conda-forge/mush-server
[conda-link]:               https://github.com/conda-forge/mush-server-feedstock
[github-discussions-badge]: https://img.shields.io/static/v1?label=Discussions&message=Ask&color=blue&logo=github
[github-discussions-link]:  https://gitlab.com/gadoue/mush-server/discussions
[gitter-badge]:             https://badges.gitter.im/https://gitlab.com/gadoue/mush-server/community.svg
[gitter-link]:              https://gitter.im/https://gitlab.com/gadoue/mush-server/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge
[pypi-link]:                https://pypi.org/project/mush-server/
[pypi-platforms]:           https://img.shields.io/pypi/pyversions/mush-server
[pypi-version]:             https://badge.fury.io/py/mush-server.svg
[rtd-badge]:                https://readthedocs.org/projects/mush-server/badge/?version=latest
[rtd-link]:                 https://mush-server.readthedocs.io/en/latest/?badge=latest
[sk-badge]:                 https://scikit-hep.org/assets/images/Scikit--HEP-Project-blue.svg

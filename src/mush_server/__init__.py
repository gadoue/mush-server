"""
Copyright (c) 2022 Guillaume Gay. All rights reserved.

mush-server: Control server for the mush microscope
"""


from __future__ import annotations

from ._version import version as __version__

__all__ = ("__version__",)

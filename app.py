from labthings import Server, create_app

# from mush_server.motors.things import Configuration, Motors
# from mush_server.motors.views import Axes, Position, Step
from mush_server.usb_camera.things import USBCamera
from mush_server.usb_camera.views import AquireAverage, ResolutionProperty


def create_olf_app(cam_idx):
    # Create LabThings Flask app
    app, labthing = create_app(
        __name__,
        title="Open Lab Frame",
        description="Test LabThing-based API",
        version="0.1.0",
    )

    my_camera = USBCamera(cam_idx)
    labthing.add_component(my_camera, "org.mush.usb_camera")

    # Add routes for the API views we created
    labthing.add_view(ResolutionProperty, "/resolution")
    labthing.add_view(AquireAverage, "/actions/average")
    return app, labthing


app, labthing = create_olf_app(cam_idx=3)
Server(app).run()
